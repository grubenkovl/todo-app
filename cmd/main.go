package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/grubenkovl/todo"
	"gitlab.com/grubenkovl/todo/pkg/handler"
	"gitlab.com/grubenkovl/todo/pkg/repository"
	"gitlab.com/grubenkovl/todo/pkg/service"
	"log"
)

func main() {
	//if err := InitConfig(); err != nil {
	//	logrus.Fatalf("eror initializating configs: %s", err.Error())
	//}

	db, err := repository.NewPostgresDB()
	if err != nil {
		log.Fatalf("failed to initialize db: %s", err.Error())
	}

	repos := repository.NewRepository(db)
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	//port := os.Getenv("PORT")
	//if port == "" {
	//	port = "8080"
	//}

	port := "8080"

	srv := new(todo.Server)
	if err := srv.Run(port, handlers.InitRoute()); err != nil {
		log.Fatalf("error occured while running http server: %s", err.Error())
	}
}

func InitConfig() error {
	return godotenv.Load()
}
