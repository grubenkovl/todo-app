package service

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/grubenkovl/todo"
	"gitlab.com/grubenkovl/todo/pkg/repository"
)

type Authorization interface {
	CreateUser(user todo.User) (int, error)
	GenerateAccessToken(username, password string) (string, error)
	GenerateAccessTokenById(uid int) (string, error)
	GenerateRefreshToken(fingerprint string, uid int) (string, error)
	ParseToken(token string) (int, error)
	Refresh(c *gin.Context, refreshToken string, fingerprint string) (string, error)
}

type TodoItem interface {
}

type Service struct {
	Authorization
	TodoItem
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Authorization: NewAuthService(repos.Authorization),
	}
}
