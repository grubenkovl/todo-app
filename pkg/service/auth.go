package service

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/grubenkovl/todo"
	"gitlab.com/grubenkovl/todo/pkg/repository"
	"math/rand"
	"os"
	"strconv"
	"time"
)

const (
	salt = "dqwd92rd138ndn13dnsc2313"
)

type AuthService struct {
	repo repository.Authorization
}

type tokenClaims struct {
	jwt.StandardClaims
	UserId int `json:"user_id"`
}

func NewAuthService(repo repository.Authorization) *AuthService {
	return &AuthService{repo: repo}
}

func (s *AuthService) CreateUser(user todo.User) (int, error) {
	user.Password = generatePasswordHash(user.Password)
	return s.repo.CreateUser(user)
}

func (s *AuthService) GenerateAccessToken(username, password string) (string, error) {
	user, err := s.repo.GetUser(username, generatePasswordHash(password))
	if err != nil {
		return "", err
	}

	if user.Id == 0 {
		return "", errors.New("not valid")
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(30 * time.Minute).Unix(),
			IssuedAt:  time.Now().Unix()},
		user.Id,
	})

	return token.SignedString([]byte(os.Getenv("jwt_signing_key")))
}

func (s *AuthService) GenerateAccessTokenById(uid int) (string, error) {
	user, err := s.repo.GetUserById(uid)
	if err != nil {
		return "", err
	}

	if user.Id == 0 {
		return "", errors.New("not valid")
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(30 * time.Minute).Unix(),
			IssuedAt:  time.Now().Unix()},
		user.Id,
	})

	return token.SignedString([]byte(os.Getenv("jwt_signing_key")))
}

func (s *AuthService) GenerateRefreshToken(fingerprint string, uid int) (string, error) {
	b := make([]byte, 32)

	sour := rand.NewSource(time.Now().Unix())
	r := rand.New(sour)

	_, err := r.Read(b)
	if err != nil {
		return "", err
	}

	rt := fmt.Sprintf("%x", b)

	err = s.repo.CreateRefreshToken(rt, fingerprint, uid)
	if err != nil {
		return "", err
	}

	return rt, nil
}

func (s *AuthService) ParseToken(accessToken string) (int, error) {
	token, err := jwt.ParseWithClaims(accessToken, &tokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("invalid signing method")
		}

		return []byte(os.Getenv("jwt_signing_key")), nil
	})

	if err != nil {
		return 0, err
	}

	claims, ok := token.Claims.(*tokenClaims)
	if !ok {
		return 0, errors.New("token claim aren't of type TokenClaims")
	}

	return claims.UserId, nil
}

func (s *AuthService) Refresh(c *gin.Context, refreshToken string, fingerprint string) (string, error) {
	uid, ok := c.Get("user_id")
	if !ok {
		return "", errors.New("invalid id")
	}
	userId, err := strconv.Atoi(fmt.Sprintf("%v", uid))
	if err != nil {
		return "", errors.New("invalid id")
	}

	err = s.repo.CheckRefresh(refreshToken, fingerprint)
	if err != nil {
		return "", err
	}

	user, err := s.repo.GetUserById(userId)
	if err != nil {
		return "", errors.New("invalid User id")
	}

	accessToken, err := s.GenerateAccessTokenById(user.Id)
	if err != nil {
		return "", err
	}

	return accessToken, nil
}

func generatePasswordHash(password string) string {
	hash := sha1.New()
	hash.Write([]byte(password))

	return fmt.Sprintf("%x", hash.Sum([]byte(salt)))
}
