package repository

import (
	"gitlab.com/grubenkovl/todo"
	"gorm.io/gorm"
)

type Authorization interface {
	CreateUser(user todo.User) (int, error)
	GetUser(username, password string) (todo.User, error)
	CreateRefreshToken(refreshToken string, fingerprint string, uid int) error
	GetUserById(userId int) (todo.User, error)
	CheckRefresh(refreshToken, fingerprint string) error
}

type TodoItem interface {
}

type Repository struct {
	Authorization
	TodoItem
}

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		Authorization: NewAuthPostgres(db),
	}
}
