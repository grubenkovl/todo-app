package repository

import (
	"errors"
	"gitlab.com/grubenkovl/todo"
	"gorm.io/gorm"
)

type AuthPostgres struct {
	db *gorm.DB
}

func NewAuthPostgres(db *gorm.DB) *AuthPostgres {
	return &AuthPostgres{db: db}
}

func (r *AuthPostgres) CreateUser(user todo.User) (int, error) {

	result := r.db.Create(&user)

	return user.Id, result.Error
}

func (r *AuthPostgres) GetUser(username, password string) (todo.User, error) {
	var user todo.User
	result := r.db.Where("user_name = ? AND password = ?", username, password).Find(&user)

	return user, result.Error
}

func (r *AuthPostgres) GetUserById(userId int) (todo.User, error) {
	var user todo.User
	result := r.db.Where("id = ?", userId).Find(&user)

	return user, result.Error
}

type RefreshSession struct {
	RefreshToken string
	Fingerprint  string
	UserId       int
}

func (r *AuthPostgres) CreateRefreshToken(refreshToken string, fingerprint string, uid int) error {
	rt := RefreshSession{
		RefreshToken: refreshToken,
		Fingerprint:  fingerprint,
		UserId:       uid,
	}

	result := r.db.Create(&rt)

	return result.Error
}

func (r *AuthPostgres) CheckRefresh(refreshToken string, fingerprint string) error {
	var rs RefreshSession
	r.db.Where("refresh_token = ? AND fingerprint = ?", refreshToken, fingerprint).Find(&rs)

	if refreshToken != rs.RefreshToken || fingerprint != rs.Fingerprint {
		return errors.New("invalid RefreshToken or Fingerprint")
	}
	return nil
}
