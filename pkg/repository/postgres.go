package repository

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func NewPostgresDB() (*gorm.DB, error) {
	//db, err := gorm.Open(postgres.Open(fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s",
	//	os.Getenv("db_host"),
	//	os.Getenv("db_user"),
	//	os.Getenv("db_name"),
	//	os.Getenv("db_pass"),
	//)))
	db, err := gorm.Open(postgres.Open(fmt.Sprintf("host=postgresql port=5432 user=postgres dbname=todo sslmode=disable password=postgres")))
	if err != nil {
		return nil, err
	}

	return db, nil
}
