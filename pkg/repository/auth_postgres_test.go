package repository

import (
	"fmt"
	"github.com/go-playground/assert/v2"
	"gitlab.com/grubenkovl/todo"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
	"testing"
	"time"

	_ "github.com/lib/pq"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	log "github.com/sirupsen/logrus"
)

var testDb *gorm.DB

func TestMain(m *testing.M) {
	// uses a sensible default on windows (tcp/http) and linux/osx (socket)
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	// pulls an image, creates a container based on it and runs it
	resource, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "11",
		Env: []string{
			"POSTGRES_PASSWORD=secret",
			"POSTGRES_USER=user_name",
			"POSTGRES_DB=dbname",
			"listen_addresses = '*'",
		},
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{Name: "no"}
	})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	port := resource.GetPort("5432/tcp")
	//databaseUrl := fmt.Sprintf("postgres://user_name:secret@%s/dbname?sslmode=disable", hostAndPort)
	databaseUrl := fmt.Sprintf("host=localhost port=%s user=user_name dbname=dbname sslmode=disable password=secret", port)

	log.Println("Connecting to database on url: ", databaseUrl)

	resource.Expire(120) // Tell docker to hard kill the container in 120 seconds

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	pool.MaxWait = 120 * time.Second
	if err = pool.Retry(func() error {
		testDb, err = gorm.Open(postgres.Open(databaseUrl))
		if err != nil {
			return err
		}
		sqlDB, err := testDb.DB()
		if err != nil {
			return err
		}
		return sqlDB.Ping()
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}
	//Run tests
	code := m.Run()

	// You can't defer this because os.Exit doesn't care for defer
	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}

func TestRepository_createUser(t *testing.T) {

	TestAuthPostgres := NewAuthPostgres(testDb)
	testUser := todo.User{
		Name:     "test",
		UserName: "test1",
		Password: "secret",
	}

	err := testDb.AutoMigrate(&todo.User{})
	if err != nil {
		return
	}
	user, err := TestAuthPostgres.CreateUser(testUser)
	if err != nil {
		return
	}

	var test2 todo.User

	testDb.Find(&test2)

	assert.Equal(t, test2.Id, user)
	assert.Equal(t, test2.Name, testUser.Name)
	assert.Equal(t, test2.UserName, testUser.UserName)
	assert.Equal(t, test2.Password, testUser.Password)
}

func TestRepository_getUser(t *testing.T) {

	TestAuthPostgres := NewAuthPostgres(testDb)
	testUser := todo.User{
		Name:     "test",
		UserName: "test1",
		Password: "secret",
	}

	err := testDb.AutoMigrate(&todo.User{})
	if err != nil {
		return
	}

	testDb.Create(&testUser)

	user, err := TestAuthPostgres.GetUser(testUser.UserName, testUser.Password)
	if err != nil {
		return
	}

	assert.Equal(t, user.Id, 1)
	assert.Equal(t, user.Name, testUser.Name)
	assert.Equal(t, user.UserName, testUser.UserName)
	assert.Equal(t, user.Password, testUser.Password)
}

func TestRepository_getUserById(t *testing.T) {

	TestAuthPostgres := NewAuthPostgres(testDb)
	testUser := todo.User{
		Name:     "test",
		UserName: "test1",
		Password: "secret",
	}

	err := testDb.AutoMigrate(&todo.User{})
	if err != nil {
		return
	}

	testDb.Create(&testUser)

	user, err := TestAuthPostgres.GetUserById(1)
	if err != nil {
		return
	}

	assert.Equal(t, user.Id, 1)
	assert.Equal(t, user.Name, testUser.Name)
	assert.Equal(t, user.UserName, testUser.UserName)
	assert.Equal(t, user.Password, testUser.Password)
}

func TestRepository_createRefreshToken(t *testing.T) {
	TestAuthPostgres := NewAuthPostgres(testDb)
	rs := RefreshSession{
		RefreshToken: "refresh",
		Fingerprint:  "finger",
		UserId:       1,
	}

	err := testDb.AutoMigrate(&RefreshSession{})
	if err != nil {
		return
	}

	err = TestAuthPostgres.CreateRefreshToken("refresh", "finger", 1)
	if err != nil {
		return
	}

	var testRs RefreshSession

	testDb.Find(&testRs)

	assert.Equal(t, testRs.RefreshToken, rs.RefreshToken)
	assert.Equal(t, testRs.UserId, rs.UserId)
	assert.Equal(t, testRs.Fingerprint, rs.Fingerprint)
}

func TestRepository_checkRefresh(t *testing.T) {
	TestAuthPostgres := NewAuthPostgres(testDb)
	rs := RefreshSession{
		RefreshToken: "refresh",
		Fingerprint:  "finger",
		UserId:       1,
	}

	err := testDb.AutoMigrate(&RefreshSession{})
	if err != nil {
		return
	}

	testDb.Create(&rs)

	err = TestAuthPostgres.CheckRefresh("refresh", "finger")
	if err != nil {
		t.Fail()
		return
	}
}
