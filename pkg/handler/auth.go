package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/grubenkovl/todo"
	"net/http"
)

func (h *Handler) signUp(c *gin.Context) {
	var input todo.User

	if err := c.BindJSON(&input); err != nil {
		NewErrorResponse(c, http.StatusBadRequest, "invalid input body")
		return
	}

	id, err := h.services.Authorization.CreateUser(input)
	if err != nil {
		NewErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"id": id,
	})
}

type signInInput struct {
	UserName string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func (h *Handler) signIn(c *gin.Context) {
	var input signInInput

	if err := c.BindJSON(&input); err != nil {
		NewErrorResponse(c, http.StatusBadRequest, "invalid input body")
		return
	}

	accessToken, err := h.services.Authorization.GenerateAccessToken(input.UserName, input.Password)
	if err != nil {
		NewErrorResponse(c, http.StatusInternalServerError, "trouble with access token")
		return
	}

	userId, err := h.services.Authorization.ParseToken(accessToken)
	if err != nil {
		NewErrorResponse(c, http.StatusInternalServerError, "invalid access token")
		return
	}

	refreshToken, err := h.services.Authorization.GenerateRefreshToken("fingerprint", userId)
	if err != nil {
		NewErrorResponse(c, http.StatusInternalServerError, "invalid refresh token")
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"Access Token":  accessToken,
		"Refresh Token": refreshToken,
	})
}

type refreshInput struct {
	RefreshToken string `json:"refresh_token" binding:"required"`
	Fingerprint  string `json:"fingerprint" binding:"required"`
}

func (h *Handler) refresh(c *gin.Context) {
	var input refreshInput

	if err := c.BindJSON(&input); err != nil {
		NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	accessToken, err := h.services.Refresh(c, input.RefreshToken, input.Fingerprint)
	if err != nil {
		NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"Access Token":  accessToken,
		"Refresh Token": input.RefreshToken,
	})
}
