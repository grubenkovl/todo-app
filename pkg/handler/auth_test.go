package handler

import (
	"bytes"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"github.com/golang/mock/gomock"
	"gitlab.com/grubenkovl/todo"
	"gitlab.com/grubenkovl/todo/pkg/service"
	mock_service "gitlab.com/grubenkovl/todo/pkg/service/mocks"
	"net/http/httptest"
	"testing"
)

func TestHandler_signUp(t *testing.T) {
	type mockBehavior func(s *mock_service.MockAuthorization, user todo.User)

	testTable := []struct {
		name                string
		inputBody           string
		inputUser           todo.User
		mockBehavior        mockBehavior
		expectedStatusCode  int
		expectedRequestBody string
	}{
		{
			name:      "OK",
			inputBody: `{"name":"Test","username":"test","password":"password"}`,
			inputUser: todo.User{
				Name:     "Test",
				UserName: "test",
				Password: "password",
			},
			mockBehavior: func(s *mock_service.MockAuthorization, user todo.User) {
				s.EXPECT().CreateUser(user).Return(1, nil)
			},
			expectedStatusCode:  200,
			expectedRequestBody: `{"id":1}`,
		},
		{
			name:                "Empty Fields",
			inputBody:           `{"password":"password"}`,
			inputUser:           todo.User{},
			mockBehavior:        func(s *mock_service.MockAuthorization, user todo.User) {},
			expectedStatusCode:  400,
			expectedRequestBody: `{"message":"invalid input body"}`,
		},
		{
			name:      "Service error",
			inputBody: `{"name":"Test","username":"test","password":"password"}`,
			inputUser: todo.User{
				Name:     "Test",
				UserName: "test",
				Password: "password",
			},
			mockBehavior: func(s *mock_service.MockAuthorization, user todo.User) {
				s.EXPECT().CreateUser(user).Return(0, errors.New("something went wrong"))
			},
			expectedStatusCode:  500,
			expectedRequestBody: `{"message":"something went wrong"}`,
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.name, func(t *testing.T) {
			c := gomock.NewController(t)
			defer c.Finish()

			auth := mock_service.NewMockAuthorization(c)
			testCase.mockBehavior(auth, testCase.inputUser)

			services := &service.Service{Authorization: auth}
			handler := Handler{services}

			r := gin.New()
			r.POST("/sign-up", handler.signUp)

			w := httptest.NewRecorder()
			req := httptest.NewRequest("POST", "/sign-up",
				bytes.NewBufferString(testCase.inputBody))

			r.ServeHTTP(w, req)

			assert.Equal(t, testCase.expectedStatusCode, w.Code)
			assert.Equal(t, testCase.expectedRequestBody, w.Body.String())
		})
	}
}

func TestHandler_signIn(t *testing.T) {
	type mockAccess func(s *mock_service.MockAuthorization, username string, password string)
	type mockRefresh func(s *mock_service.MockAuthorization, fingerprint string, uid int)
	type mockParse func(s *mock_service.MockAuthorization, accessToken string)

	testTable := []struct {
		name                string
		inputBody           string
		mockAccess          mockAccess
		mockRefresh         mockRefresh
		mockParse           mockParse
		expectedStatusCode  int
		expectedRequestBody string
	}{
		{
			name:      "OK",
			inputBody: `{"username":"test","password":"password"}`,
			mockAccess: func(s *mock_service.MockAuthorization, username string, password string) {
				s.EXPECT().GenerateAccessToken(username, password).Return("access", nil)
			},
			mockRefresh: func(s *mock_service.MockAuthorization, fingerprint string, uid int) {
				s.EXPECT().GenerateRefreshToken(fingerprint, uid).Return("refresh", nil)
			},
			mockParse: func(s *mock_service.MockAuthorization, accessToken string) {
				s.EXPECT().ParseToken(accessToken).Return(1, nil)
			},
			expectedStatusCode:  200,
			expectedRequestBody: `{"Access Token":"access","Refresh Token":"refresh"}`,
		},
		{
			name:                "Empty Fields",
			inputBody:           `{"username":"test"}`,
			mockAccess:          func(s *mock_service.MockAuthorization, username string, password string) {},
			mockRefresh:         func(s *mock_service.MockAuthorization, fingerprint string, uid int) {},
			mockParse:           func(s *mock_service.MockAuthorization, accessToken string) {},
			expectedStatusCode:  400,
			expectedRequestBody: `{"message":"invalid input body"}`,
		},
		{
			name:      "Wrong access",
			inputBody: `{"username":"test","password":"password"}`,
			mockAccess: func(s *mock_service.MockAuthorization, username string, password string) {
				s.EXPECT().GenerateAccessToken(username, password).Return("", errors.New(""))
			},
			mockRefresh:         func(s *mock_service.MockAuthorization, fingerprint string, uid int) {},
			mockParse:           func(s *mock_service.MockAuthorization, accessToken string) {},
			expectedStatusCode:  500,
			expectedRequestBody: `{"message":"trouble with access token"}`,
		},
		{
			name:      "Invalid parse token",
			inputBody: `{"username":"test","password":"password"}`,
			mockAccess: func(s *mock_service.MockAuthorization, username string, password string) {
				s.EXPECT().GenerateAccessToken(username, password).Return("access", nil)
			},
			mockRefresh: func(s *mock_service.MockAuthorization, fingerprint string, uid int) {},
			mockParse: func(s *mock_service.MockAuthorization, accessToken string) {
				s.EXPECT().ParseToken(accessToken).Return(0, errors.New(""))
			},
			expectedStatusCode:  500,
			expectedRequestBody: `{"message":"invalid access token"}`,
		},
		{
			name:      "Wrong refresh token",
			inputBody: `{"username":"test","password":"password"}`,
			mockAccess: func(s *mock_service.MockAuthorization, username string, password string) {
				s.EXPECT().GenerateAccessToken(username, password).Return("access", nil)
			},
			mockRefresh: func(s *mock_service.MockAuthorization, fingerprint string, uid int) {
				s.EXPECT().GenerateRefreshToken(fingerprint, uid).Return("", errors.New(""))
			},
			mockParse: func(s *mock_service.MockAuthorization, accessToken string) {
				s.EXPECT().ParseToken(accessToken).Return(1, nil)
			},
			expectedStatusCode:  500,
			expectedRequestBody: `{"message":"invalid refresh token"}`,
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.name, func(t *testing.T) {
			c := gomock.NewController(t)
			defer c.Finish()

			auth := mock_service.NewMockAuthorization(c)
			testCase.mockAccess(auth, "test", "password")
			testCase.mockParse(auth, "access")
			testCase.mockRefresh(auth, "fingerprint", 1)

			services := &service.Service{Authorization: auth}
			handler := Handler{services}

			r := gin.New()
			r.POST("/sign-in", handler.signIn)

			w := httptest.NewRecorder()
			req := httptest.NewRequest("POST", "/sign-in",
				bytes.NewBufferString(testCase.inputBody))

			r.ServeHTTP(w, req)

			assert.Equal(t, testCase.expectedStatusCode, w.Code)
			assert.Equal(t, testCase.expectedRequestBody, w.Body.String())
		})
	}
}
