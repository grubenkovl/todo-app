package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func (h *Handler) createItem(c *gin.Context) {
	id, _ := c.Get("user_id")
	c.JSON(http.StatusOK, map[string]interface{}{
		"id": id,
	})
}

func (h *Handler) getAllItems(c *gin.Context) {

}
func (h *Handler) getItemById(c *gin.Context) {

}
func (h *Handler) updateItem(c *gin.Context) {

}
func (h *Handler) deleteItem(c *gin.Context) {

}
