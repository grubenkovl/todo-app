CREATE TABLE users
(
    id        serial       not null unique,
    name      varchar(255) not null,
    user_name varchar(255) not null unique,
    password  varchar(255) not null
);

CREATE TABLE todo_item
(
    id          serial                                      not null unique,
    title       varchar(255)                                not null,
    description varchar(255),
    user_id     int references users (id) on delete cascade not null
);

CREATE TABLE refresh_sessions
(
    id            serial                                      not null unique,
    refresh_token varchar(255)                                not null,
    user_id       int references users (id) on delete cascade not null,
    fingerprint   varchar(255)                                not null
);