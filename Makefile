migrate:
	migrate -path ./schema -database 'postgres://postgres:445807@localhost:5432/todotwo?sslmode=disable' up

test:
	go test -timeout 1m -v ./...


gen:
	mockgen -source=pkg/service/service.go \
	-destination=pkg/service/mocks/mock_service.go
