package todo

type TodoItem struct {
	Id          int    `json:"id"`
	Title       string `json:"title" binding:"required"`
	Description string `json:"description" binding:"required"`
	UserId      int    `json:"userId" binding:"required"`
}
